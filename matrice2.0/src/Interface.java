

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.ejml.simple.SimpleMatrix;

import javax.swing.SwingConstants;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ScrollPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Rectangle;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.MatteBorder;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.ComponentOrientation;
import java.awt.Component;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.BevelBorder;

public class Interface {

	private JFrame frame;
	private JFormattedTextField mat1Text;
	private JFormattedTextField mat2Text;
	private JLabel lblNewLabel;
	private JLabel lblMatrice;
	private JButton btnNewButton;
	private MyJTable jtab1;
	private MyJTable jtab2;
	private JComboBox comboBox;
	private JTextField scalaire1;
	private JTextField scalaire2;
	private JRadioButton rdbtnInverse1;
	private JRadioButton rdbtnInverse2;
	private JButton btnScalaire_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JPanel panel;
	private JLabel lblNewLabel_3;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		frame.setSize(new Dimension(1920, 1080));
		frame.setBounds(100, 100, 1920, 1080);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		mat1Text = new JFormattedTextField();
		mat1Text.setBounds(41, 48, 90, 19);
		
		mat2Text = new JFormattedTextField();
		mat2Text.setBounds(41, 102, 90, 19);
		
		lblNewLabel = new JLabel("Matrice 1\r\n");
		lblNewLabel.setBounds(41, 24, 90, 13);
		lblNewLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblMatrice = new JLabel("Matrice 2");
		lblMatrice.setBounds(41, 78, 90, 13);
		lblMatrice.setHorizontalAlignment(SwingConstants.CENTER);
		lblMatrice.setHorizontalTextPosition(SwingConstants.CENTER);
		
		btnNewButton = new JButton("");
		btnNewButton.setToolTipText("Initier les dimensions");
		btnNewButton.setBounds(137, 102, 50, 23);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String mat1String=mat1Text.getText().replace('x', ',');				
				String mat2String=mat2Text.getText().replace('x', ',');
				
				try {
				int x=Integer.parseInt(mat1String.substring(0, mat1String.indexOf(',')));
				int y=Integer.parseInt(mat1String.substring(mat1String.indexOf(',')+1, mat1String.length()));
				jtab1.set(new MyJTable(x,y));
				} catch (Exception er) {
					
				}
				
				try {
				int x=Integer.parseInt(mat2String.substring(0, mat2String.indexOf(',')));
				int y=Integer.parseInt(mat2String.substring(mat2String.indexOf(',')+1, mat2String.length()));
				jtab2.set(new MyJTable(x,y));
				} catch (Exception er) {
					
				}
				mat1Text.setText("");
				mat2Text.setText("");
			}
		});
		
		comboBox = new JComboBox();
		comboBox.setBounds(41, 132, 90, 19);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Addition", "Substraction", "Multiplication", "Gauss-Jordan"}));
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setBounds(137, 132, 50, 23);
		btnNewButton_1.setAlignmentX(Component.CENTER_ALIGNMENT);
	
		JRadioButton rdbtnPrint2 = new JRadioButton("");
		rdbtnPrint2.setAlignmentX(0.5f);
		rdbtnPrint2.setActionCommand("");
		rdbtnPrint2.setBounds(657, 131, 21, 21);
		frame.getContentPane().add(rdbtnPrint2);
		
		JRadioButton rdbtnPrint1 = new JRadioButton("\r\n");
		rdbtnPrint1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				rdbtnPrint2.setSelected(false);
			}
		});
		rdbtnPrint1.setAlignmentX(0.5f);
		rdbtnPrint1.setActionCommand("");
		rdbtnPrint1.setBounds(632, 131, 25, 21);
		
		rdbtnPrint2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnPrint1.setSelected(false);
			}
		});
		frame.getContentPane().add(rdbtnPrint1);
		
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				verif();
				String op=comboBox.getSelectedItem().toString();
				SimpleMatrix mat;
				if (op.equals("Addition")) {
					if (jtab1.getRowCount()==jtab2.getRowCount() && jtab1.getColumnCount()==jtab2.getColumnCount()) {
						mat=new SimpleMatrix(jtab1.toTab());
						SimpleMatrix mat2=new SimpleMatrix(jtab2.toTab());
						mat.equation("A=A+B",mat2,"B");
						
						if (!rdbtnPrint1.isSelected() && !rdbtnPrint2.isSelected()) {
						JOptionPane.showMessageDialog(null, mat.toString());
						System.out.println(mat.toString());
						} else {
							if (rdbtnPrint1.isSelected()) {
								jtab1.setValues(mat);
							} else {
								if (rdbtnPrint2.isSelected()) {
								jtab2.setValues(mat);
								}
							}
						}
					} else {
						JOptionPane.showMessageDialog(null,"Matrices non compatibles");
					}
				} else {
					if (op.equals("Substraction")) {
						if (jtab1.getRowCount()==jtab2.getRowCount() && jtab1.getColumnCount()==jtab2.getColumnCount()) {
							mat=new SimpleMatrix(jtab1.toTab());
							SimpleMatrix mat2=new SimpleMatrix(jtab2.toTab());
							mat.equation("A=A-B",mat2,"B");
							
							if (!rdbtnPrint1.isSelected() && !rdbtnPrint2.isSelected()) {
							JOptionPane.showMessageDialog(null, mat.toString());
							System.out.println(mat.toString());
							} else {
								if (rdbtnPrint1.isSelected()) {
									jtab1.setValues(mat);
								} else {
									if (rdbtnPrint2.isSelected()) {
										jtab2.setValues(mat);
									}
								}
							}
						} else {
							JOptionPane.showMessageDialog(null,"Matrices non compatibles");
						}
					} else {
							if (op.equals("Multiplication")) {
								if (jtab1.getColumnCount()==jtab2.getRowCount()) {
									SimpleMatrix mat1=new SimpleMatrix(jtab1.toTab());
									SimpleMatrix mat2=new SimpleMatrix(jtab2.toTab());
									mat=mat1.mult(mat2);
									
									if (!rdbtnPrint1.isSelected() && !rdbtnPrint2.isSelected()) {
										JOptionPane.showMessageDialog(null, mat.toString());
										System.out.println(mat.toString());
										} else {
											if (rdbtnPrint1.isSelected()) {
												jtab1.setValues(mat);
											} else {
												if (rdbtnPrint2.isSelected()) {
													jtab2.setValues(mat);
												}
											}
										}
								} else {
									JOptionPane.showMessageDialog(null,"Matrices non compatibles");
								}
						} else {
							if (op.equals("Gauss-Jordan")) {
								String entry=JOptionPane.showInputDialog("Matrice");
								MyJTable temp=new MyJTable(3,3);
								if (entry.contains("1")) {
									temp=gaussJordan(jtab1);
								} else {
									if (entry.contains("2")) {
										temp=gaussJordan(jtab2);
									}
								}
								mat=new SimpleMatrix(temp.toTab());
							if (!rdbtnPrint1.isSelected() && !rdbtnPrint2.isSelected()) {
							JOptionPane.showMessageDialog(null,mat.toString());
							System.out.println(mat.toString());
							} else {
								if (rdbtnPrint1.isSelected()) {
									jtab1.set(temp);
								} else {
									if (rdbtnPrint2.isSelected()) {
										jtab2.set(temp);
									}
								}
							}
							}
						}
					}
				}
			}
		});
		
		JRadioButton rdbtnFlip1 = new JRadioButton("\r\n");
		rdbtnFlip1.setBounds(203, 131, 25, 21);
		rdbtnFlip1.setAlignmentX(Component.CENTER_ALIGNMENT);
		rdbtnFlip1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SimpleMatrix mat=new SimpleMatrix(jtab1.toTab());
				jtab1.setValues(mat.transpose());
			}
		});
		rdbtnFlip1.setActionCommand("");
		
		JRadioButton rdbtnFlip2 = new JRadioButton("");
		rdbtnFlip2.setBounds(228, 131, 21, 21);
		rdbtnFlip2.setAlignmentX(Component.CENTER_ALIGNMENT);
		rdbtnFlip2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SimpleMatrix mat=new SimpleMatrix(jtab2.toTab());
				jtab2.setValues(mat.transpose());
			}
		});
		rdbtnFlip2.setActionCommand("");
		
		JRadioButton rdbtnFlip = new JRadioButton("");
		rdbtnFlip.setBounds(286, 132, 21, 21);
		rdbtnFlip.setAlignmentX(Component.CENTER_ALIGNMENT);
		rdbtnFlip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TableModel mat1Temp=jtab1.getModel();
				TableModel mat2Temp=jtab2.getModel();
				
				jtab1.setModel(mat2Temp);
				jtab2.setModel(mat1Temp);
			}
		});
		rdbtnFlip.setActionCommand("");
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(lblNewLabel);
		
		panel = new JPanel();
		panel.setBounds(35, 151, 1495, 633);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		jtab1=new MyJTable(3,3);
		jtab1.setBounds(6, 15, 700, 818);
		panel.add(jtab1);
		jtab1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode()==127) {
					int x=jtab1.getRowCount();
					int y=jtab1.getColumnCount();
					jtab1.set(new MyJTable(x,y));
					jtab1.clearSelection();
				}
			}
		});
		jtab1.setPreferredSize(new Dimension(913, 860));
		jtab1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		jtab2=new MyJTable(3,3);
		jtab2.setBounds(721, 15, 700, 860);
		panel.add(jtab2);
		jtab2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode()==127) {
					int x=jtab2.getRowCount();
					int y=jtab2.getColumnCount();
					jtab2.set(new MyJTable(x,y));
					jtab2.clearSelection();
				}
			}
		});
		jtab2.setPreferredSize(new Dimension(913, 860));
		jtab2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		frame.getContentPane().add(comboBox);
		frame.getContentPane().add(mat2Text);
		frame.getContentPane().add(btnNewButton_1);
		frame.getContentPane().add(rdbtnFlip1);
		frame.getContentPane().add(rdbtnFlip2);
		frame.getContentPane().add(rdbtnFlip);
		frame.getContentPane().add(btnNewButton);
		frame.getContentPane().add(mat1Text);
		frame.getContentPane().add(lblMatrice);
		
		scalaire1 = new JTextField();
		scalaire1.setBounds(340, 131, 50, 24);
		frame.getContentPane().add(scalaire1);
		scalaire1.setColumns(10);
		
		scalaire2 = new JTextField();
		scalaire2.setColumns(10);
		scalaire2.setBounds(400, 131, 50, 24);
		frame.getContentPane().add(scalaire2);
		
		JButton btnScalaire = new JButton("");
		btnScalaire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verif();
				if (!scalaire1.getText().equals("")) {
					double k=1;
					try {
						k=Double.parseDouble(scalaire1.getText());
						double[][] tab=jtab1.toTab();
						for (int c=0;c<jtab1.getRowCount();c++) {
							for (int d=0;d<jtab1.getColumnCount();d++) {
								tab[c][d]*=k;
							}
						}
						jtab1.setValues(tab);
					} catch (Exception exception) {
						System.out.println(exception);
						JOptionPane.showMessageDialog(null, "ERREUR: Un scalaire contient des caract�res");
					}
				}
				if (!scalaire2.getText().equals("")) {
					double k=1;
					try {
						k=Double.parseDouble(scalaire2.getText());
						double[][] tab=jtab2.toTab();
						for (int c=0;c<jtab2.getRowCount();c++) {
							for (int d=0;d<jtab2.getColumnCount();d++) {
								tab[c][d]*=k;
							}
						}
						jtab2.setValues(tab);
					} catch (Exception exception) {
						System.out.println(exception);
						JOptionPane.showMessageDialog(null, "ERREUR: Un scalaire contient des caract�res");
					}
				}
				scalaire1.setText("");
				scalaire2.setText("");
			}
		});
		btnScalaire.setAlignmentX(0.5f);
		btnScalaire.setBounds(460, 132, 50, 23);
		frame.getContentPane().add(btnScalaire);
		
		rdbtnInverse1 = new JRadioButton("\r\n");
		buttonGroup.add(rdbtnInverse1);
		rdbtnInverse1.setAlignmentX(0.5f);
		rdbtnInverse1.setActionCommand("");
		rdbtnInverse1.setBounds(520, 130, 25, 21);
		frame.getContentPane().add(rdbtnInverse1);
		
		rdbtnInverse2 = new JRadioButton("");
		buttonGroup.add(rdbtnInverse2);
		rdbtnInverse2.setAlignmentX(0.5f);
		rdbtnInverse2.setActionCommand("");
		rdbtnInverse2.setBounds(545, 130, 21, 21);
		frame.getContentPane().add(rdbtnInverse2);
		
		btnScalaire_1 = new JButton("");
		btnScalaire_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnInverse1.isSelected()) {
					if (jtab1.getRowCount()==jtab1.getColumnCount()) {
						SimpleMatrix mat=new SimpleMatrix(jtab1.toTab());
						jtab1.setValues(mat.invert());
					} else {
						JOptionPane.showMessageDialog(null, "La matrice selectionn� ne peut pas �tre invers�e");
					}
				} else {
					if (rdbtnInverse2.isSelected()) {
						if (jtab2.getRowCount()==jtab2.getColumnCount()) {
							SimpleMatrix mat=new SimpleMatrix(jtab2.toTab());
							jtab2.setValues(mat.invert());
						} else {
							JOptionPane.showMessageDialog(null, "La matrice selectionn� ne peut pas �tre invers�e");
						}
					}
				}
				buttonGroup.clearSelection();
			}
		});
		btnScalaire_1.setAlignmentX(0.5f);
		btnScalaire_1.setBounds(572, 129, 50, 23);
		frame.getContentPane().add(btnScalaire_1);
		
		JButton btnInput1 = new JButton("");
		btnInput1.setToolTipText("Importer Matrice 1");
		btnInput1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				jtab1.set(input(jtab1));
			}
		});
		btnInput1.setBounds(137, 47, 50, 23);
		frame.getContentPane().add(btnInput1);
		
		JButton btnInput2 = new JButton("");
		btnInput2.setToolTipText("Importer Matrice 2");
		btnInput2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jtab2.set(input(jtab2));
			}
		});
		btnInput2.setBounds(193, 47, 50, 23);
		frame.getContentPane().add(btnInput2);
		
		JButton button = new JButton("New button");
		button.setBounds(369, 526, 85, 21);
		frame.getContentPane().add(button);
		
		JLabel lblNewLabel_1 = new JLabel("Transposer");
		lblNewLabel_1.setBounds(193, 113, 67, 13);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Multiplication Scalaire\r\n");
		lblNewLabel_2.setBounds(372, 113, 137, 13);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("Inverse");
		lblNewLabel_2_1.setBounds(575, 113, 50, 13);
		frame.getContentPane().add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_2_1_1 = new JLabel("Sortie");
		lblNewLabel_2_1_1.setBounds(638, 113, 57, 13);
		frame.getContentPane().add(lblNewLabel_2_1_1);
		
		lblNewLabel_3 = new JLabel("\u00C9changer");
		lblNewLabel_3.setBounds(270, 113, 67, 13);
		frame.getContentPane().add(lblNewLabel_3);
		
	
	}
	public void verif() {
		try {
			for (int c=0;c<jtab1.getRowCount();c++) {
				for (int r=0;r<jtab1.getColumnCount();r++) {
					jtab1.setValueAt(Double.parseDouble((String)jtab1.getValueAt(c, r)), c, r);
				}
			}
		} catch (Exception er) {
			
		}
		
		try {
			for (int c=0;c<jtab2.getRowCount();c++) {
				for (int r=0;r<jtab2.getColumnCount();r++) {
					jtab2.setValueAt(Double.parseDouble((String)jtab2.getValueAt(c, r)), c, r);
				}
			}
		} catch (Exception er) {
			
		}
	}
	
	public MyJTable gaussJordan(MyJTable jtab) {
		double[][] tab=jtab.toTab();
		int x=jtab.getRowCount();
		int y=jtab.getColumnCount();
		
		
		for (int c=0;c<x;c++) {
			double tempA=tab[c][c];
			for (int d=0;d<x;d++) {
				if (c!=d) {
				double tempB=tab[d][c];
				for (int e=0;e<y;e++) {
					tab[d][e]-=tempB/tempA*tab[c][e];
				}
				}
			}
		}
		
		for (int c=0;c<x;c++) {
			double temp=tab[c][c];
			for (int d=0;d<y;d++) {
				tab[c][d]/=temp;
			}
		}
		
		return new MyJTable(tab);
	}
	
	public MyJTable input(MyJTable jtab) {
		String input = JOptionPane.showInputDialog("Matrice");
		char[] arr=input.toCharArray();
		String temp;
		int c=1;
		int l=1;
		ArrayList list=new ArrayList();
		
		for (int r=0;r<arr.length;r++) {
			if (arr[r]==' ' || arr[r]=='\t') {
				if (l==1) {
					if (arr[r]=='\t')c++;
				}
				if (arr[r]==' ')l++;
				list.add(input.substring(0, r));
				input=input.substring(r+1);
				arr=input.toCharArray();
				r=0;
			}
		}
		list.add(input);
		jtab=new MyJTable(l,c);
		
		
		Iterator it = list.iterator();
		for (int r=0;r<l;r++) {
			for (int s=0;s<c;s++) {
				jtab.setValueAt(it.next(), r, s);
			}
		}
		return jtab;
	}
}

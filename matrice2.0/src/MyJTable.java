import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.ejml.simple.SimpleMatrix;

public class MyJTable extends JTable {
	
	public MyJTable(int x, int y) {
	this.setModel(new DefaultTableModel(x,y));
	}
	
	public MyJTable(double[][] tab) {
	int x=tab.length;
	int y=tab[0].length;
	
	this.setModel(new DefaultTableModel(x,y));
	
	setValues(tab);
	}
	
	
	public void setValues(double[][] tab) {
		int x=tab.length;
		int y=tab[0].length;
		
		this.setModel(new DefaultTableModel(x,y));
		
		for (int c=0;c<tab.length;c++) {
			for (int d=0;d<tab[0].length;d++) {
				this.setValueAt(tab[c][d], c, d);
			}
		}
	}
	
	public void setValues(SimpleMatrix mat) {
		int x=mat.numRows();
		int y=mat.numCols();
		
		this.setModel(new DefaultTableModel(x,y));
		
		for (int c=0;c<x;c++) {
			for (int d=0;d<y;d++) {
				this.setValueAt(mat.get(c, d), c, d);
			}
		}
	}
	
	public double[][] toTab(){
		int x=this.getRowCount();
		int y=this.getColumnCount();
		
		double[][] res=new double[x][y];
		
		for (int c=0;c<x;c++) {
			for (int d=0;d<y;d++) {
				Object temp=this.getValueAt(c, d);
				if (temp.getClass().getSimpleName().equals("String")) {
					if (!temp.equals("")) {
					res[c][d]=Double.parseDouble((String)temp);
					} else {
						res[c][d]=0;
					}
				} else {
					res[c][d]=(Double)temp;
				}
			}
		}
		return res;
	}
	
	public void set(MyJTable jtab) {
		this.setModel(jtab.getModel());
	}
	
	public void removeRow(int index) {
		double[][] tab=this.toTab();
		int x=tab.length;
		int y=tab[0].length;
		
		double[][] res=new double[x-1][y];
		
		for (int c=0;c<x;c++) {
			for (int d=0;d<y;d++) {
				if (c<index) {
					res[c][d]=tab[c][d];
				} else {
					if (c>index) {
						res[c-1][d]=tab[c][d];
					}
				}
			}
		}
		this.setValues(res);
	}
	
	public void removeColumn(int index) {
		double[][] tab=this.toTab();
		int x=tab.length;
		int y=tab[0].length;
		
		double[][] res=new double[x][y-1];
		
		for (int c=0;c<x;c++) {
			for (int d=0;d<y;d++) {
				if (d<index) {
					res[c][d]=tab[c][d];
				} else {
					if (d>index) {
						res[c][d-1]=tab[c][d];
					}
				}
			}
		}
		this.setValues(res);
	}
}
